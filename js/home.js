function validateMentorLogin(){
    if(firebase.auth().currentUser) {
        window.location.href = "mentor.html";
    } else {
        window.location.href = "login.html";
    }
}

function validateMenteeLogin(){
    if(firebase.auth().currentUser) {
        window.location.href = "mentee.html";
    } else {
        window.location.href = "login.html";
    }
}

// Add a realtime listener
firebase.auth().onAuthStateChanged(firebaseUser => {
    if(firebaseUser) {
        console.log('Logged In User : ', firebaseUser);
        navLogIn.classList.add('d-none');
        navLogOut.classList.remove('d-none');
    } else {
        console.log('Not logged In');
        navLogIn.classList.remove('d-none');
        navLogOut.classList.add('d-none');
    }
});
