var googleProvider = new firebase.auth.GoogleAuthProvider();
googleProvider.addScope('https://www.googleapis.com/auth/contacts.readonly');

var inputEmail = document.getElementById('inputEmail');
var inputPassword = document.getElementById('inputPassword');
var btnLogIn = document.getElementById('btnLogIn');
var btnSignUp = document.getElementById('btnSignUp');
var btnLogOut = document.getElementById('btnLogOut');
var btnGoogleSignIn = document.getElementById('btnGoogleSignIn');

if(btnLogIn) {
    btnLogIn.addEventListener('click', e => {
        const email = inputEmail.value;
        const password = inputPassword.value;
        const auth = firebase.auth();
        // Log in
        const promise = auth.signInWithEmailAndPassword(email, password);
        promise.catch(e => console.log(e.message));
    });
}
if(btnSignUp) {
    btnSignUp.addEventListener('click', e => {
        // check for real email
        const email = inputEmail.value;
        const password = inputPassword.value;
        // sign in
        const promise = firebase.auth().createUserWithEmailAndPassword(email, password);
        promise
            .catch(e => console.log(e.message));
    });
}

// Add a realtime listener
firebase.auth().onAuthStateChanged(firebaseUser => {
    if(firebaseUser) {
        console.log('Logged In User : ', firebaseUser);
        window.location.href = "home.html";
    } else {
        console.log('Not logged In');
    }
});


if(btnGoogleSignIn) {
    btnGoogleSignIn.addEventListener('click', e => {
        signInUsingGoogle();
    });
}
function signInUsingGoogle() {
    // Step 1.
    // User tries to sign in to Google.
    firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider()).catch(function(error) {
      // An error happened.
      if (error.code === 'auth/account-exists-with-different-credential') {
        // Step 2.
        // User's email already exists.
        // The pending Google credential.
        var pendingCred = error.credential;
        // The provider account's email address.
        var email = error.email;
        // Get sign-in methods for this email.
        auth.fetchSignInMethodsForEmail(email).then(function(methods) {
          // Step 3.
          // If the user has several sign-in methods,
          // the first method in the list will be the "recommended" method to use.
          if (methods[0] === 'password') {
            // Asks the user their password.
            // In real scenario, you should handle this asynchronously.
            var password = promptUserForPassword(); // TODO: implement promptUserForPassword.
            auth.signInWithEmailAndPassword(email, password).then(function(user) {
              // Step 4a.
              return user.linkWithCredential(pendingCred);
            }).then(function() {
              // Google account successfully linked to the existing Firebase user.
              goToApp();
            });
            return;
          }
          // All the other cases are external providers.
          // Construct provider object for that provider.
          // TODO: implement getProviderForProviderId.
          var provider = getProviderForProviderId(methods[0]);
          // At this point, you should let the user know that they already has an account
          // but with a different provider, and let them validate the fact they want to
          // sign in with this provider.
          // Sign in to provider. Note: browsers usually block popup triggered asynchronously,
          // so in real scenario you should ask the user to click on a "continue" button
          // that will trigger the signInWithPopup.
          auth.signInWithPopup(provider).then(function(result) {
            // Remember that the user may have signed in with an account that has a different email
            // address than the first one. This can happen as Firebase doesn't control the provider's
            // sign in flow and the user is free to login using whichever account they own.
            // Step 4b.
            // Link to Google credential.
            // As we have access to the pending credential, we can directly call the link method.
            result.user.linkAndRetrieveDataWithCredential(pendingCred).then(function(usercred) {
              // Google account successfully linked to the existing Firebase user.
              goToApp();
            });
          });
        });
      }
    });
}

function sendPasswordReset() {
  const email = inputEmail.value;
  // [START sendpasswordemail]
  firebase.auth().sendPasswordResetEmail(email).then(function() {
    // Password Reset Email Sent!
    // [START_EXCLUDE]
    alert('Password Reset Email Sent!');
    // [END_EXCLUDE]
  }).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // [START_EXCLUDE]
    if (errorCode == 'auth/invalid-email') {
      alert(errorMessage);
    } else if (errorCode == 'auth/user-not-found') {
      alert(errorMessage);
    }
    console.log(error);
  });
}

