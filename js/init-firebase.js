(function () {
    'use strict'
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyC9u02FfydzPuMDxqUH3VbKFOItY5xU_Ss",
        authDomain: "mentoringhub-6c8b5.firebaseapp.com",
        databaseURL: "https://mentoringhub-6c8b5.firebaseio.com",
        projectId: "mentoringhub-6c8b5",
        storageBucket: "mentoringhub-6c8b5.appspot.com",
        messagingSenderId: "472284221460",
        appId: "1:472284221460:web:c341b5c15d69600e0fd701",
        measurementId: "G-PQ0ENGTYKP"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    console.log(firebase.app().name);  // "[DEFAULT]"
    //  // Use the shorthand notation to access the default project's Firebase services
    //  var defaultStorage = firebase.storage();
    //  var defaultFirestore = firebase.firestore();
}())


function logOutFunction(){
    firebase.auth().signOut();
    console.log('Logged Out');
}


